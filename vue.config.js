module.exports = {
  configureWebpack: {
    output: {
      libraryExport: "default",
    },
    externals: [
      "firebase/app",
      "firebase/auth",
      "firebase/database",
      "firebase/functions",
    ],
  },
};
