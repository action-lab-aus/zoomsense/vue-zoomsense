# vue-zoomsense

This is a small npm library to help authenticate and connect to a ZoomSense deployment using an anonymous access token. It simplifies connecting and binding to firebase data for a specific meeting.

Install: `npm install @zoomsense/vue-zoomsense`

See https://gitlab.com/action-lab-aus/zoomsense/vue-zoomsense-example for super-simple use case.

## Deps

This npm package requires `npm install firebase` and `npm install vuefire` to work. Below is how to setup the credentials to the deployment of ZoomSense.

```
import ZoomSense from "@zoomsense/vue-zoomsense";
import { rtdbPlugin } from "vuefire";

Vue.config.productionTip = false;

let config = {
  apiKey: process.env.VUE_APP_API_KEY,
  authDomain: process.env.VUE_APP_AUTH_DOMAIN,
  databaseURL: process.env.VUE_APP_DATABASE_URL,
  projectId: process.env.VUE_APP_PROJECT_ID,
  appId: process.env.VUE_APP_APP_ID,
};

Vue.use(ZoomSense, config);

Vue.use(rtdbPlugin);
```

## Building

This is a VueJS plugin. Run `npm run build` to create the dist files.
